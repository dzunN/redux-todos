import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';
import {deleteTodo, getTodos, postTodo} from '../actions';
import styles from './styles';

function Todos() {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [selectedStatus, setSelectedStatus] = useState('ON_PROGRESS');
  const dispatch = useDispatch();
  const todo = useSelector(state => state.todos);
  const {isLoading} = useSelector(state => state);

  const onSubmit = () => {
    const form = {
      title: title,
      description: description,
      status: selectedStatus,
    };
    if (getItem && getItem.id) {
      dispatch(editTodo(form, getItem.id));
      setTitle('');
      setDescription('');
      dispatch(getTodos());
    } else {
      dispatch(postTodo(form));
      setTitle('');
      setDescription('');
      dispatch(getTodos());
    }
  };

  const onDelete = id => {
    dispatch(deleteTodo(id));
    dispatch(getTodos());
  };

  const getItem = item => {
    setSelectedUser(item);
    setTitle(item.title);
    setDescription(item.description);
  };

  useEffect(() => {
    dispatch(getTodos());
  }, [dispatch]);

  return (
    <View style={styles.root}>
      <ScrollView>
        <View style={styles.headingContainer}>
          <Text style={styles.heading}>Redux Todo List</Text>
        </View>

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            onChangeText={text => setTitle(text)}
            value={title}
            placeholder="Title"
            placeholderTextColor="white"
          />
          <TextInput
            style={styles.input}
            onChangeText={text => setDescription(text)}
            value={description}
            placeholder="Description"
            placeholderTextColor="white"
          />
        </View>
        <View style={styles.statusesContainer}>
          <TouchableOpacity
            onPress={() => setSelectedStatus('ON_PROGRESS')}
            style={[
              styles.statusButton,
              selectedStatus === 'ON_PROGRESS' && styles.statusButtonSelected,
            ]}>
            <Text
              style={[
                styles.statusButtonText,
                selectedStatus === 'ON_PROGRESS' &&
                  styles.statusButtonTextSelected,
              ]}>
              Progress
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => setSelectedStatus('DONE')}
            style={[
              styles.statusButton,
              selectedStatus === 'DONE' && styles.statusButtonSelected,
            ]}>
            <Text
              style={[
                styles.statusButtonText,
                selectedStatus === 'DONE' && styles.statusButtonTextSelected,
              ]}>
              Done
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', marginLeft: 'auto', marginTop: 20}}>
          <TouchableOpacity style={styles.saveButon} onPress={onSubmit}>
            <Text style={styles.buttonText}>{getItem.id ? "Update" : "Save"}</Text>
            <Icon name="save" size={20} color="white" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.cancelButton}>
            <Text style={styles.buttonText}>Cancel</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.content}>
          <Text style={styles.todoDateText}>Monday</Text>
          {isLoading ? (
            <ActivityIndicator size="large" color="#4CCCE9" />
          ) : (
            <>
              {todo.map((itemTodo, index) => (
                <View style={styles.cardListContainer} key={index}>
                  <View style={styles.todoCard}>
                    <View style={styles.todoTitleContainer}>
                      <View style={styles.todoActionContainer}>
                        <Text style={styles.todoTitle}>{itemTodo.title}</Text>
                        <TouchableOpacity style={styles.doneBadge}>
                          <Text style={styles.doneBadgeText}>
                            {itemTodo.status}
                          </Text>
                        </TouchableOpacity>
                      </View>

                      <View style={styles.todoActionContainer}>
                        <TouchableOpacity
                          style={styles.editButton}
                          onPress={getItem(itemTodo)}>
                          <Icon name="edit" size={20} color="white" />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={onDelete(itemTodo.id)}>
                          <Icon name="trash-2" size={20} color="white" />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View>
                      <Text style={styles.todoDescription}>
                        {itemTodo.title}
                      </Text>
                    </View>
                  </View>
                </View>
              ))}
            </>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

export default Todos;
