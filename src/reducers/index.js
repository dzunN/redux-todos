import {FETCH_TODO_SUCCESS, SET_LOADING, POST_TODO, EDIT_TODO, DELETE_TODO} from '../types';

const initialState = {
  todos: [],
  isLoading: false,
};

const Reducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODO_SUCCESS:
      return {
        ...state,
        todos: action.payload,
      };
    case POST_TODO:
      return {
        ...state,
        todos:[...state.todos, action.payload]

      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.value,
      };
    default:
      return state;
  }
};

export default Reducers;
