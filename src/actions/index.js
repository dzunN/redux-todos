import axios from '../axios';
import {FETCH_TODO_SUCCESS, POST_TODO, SET_LOADING} from '../types';
import {Alert} from 'react-native';

export const saveTodos = data => ({
  type: FETCH_TODO_SUCCESS,
  payload: data,
});

export const addTodo = data => ({
  type: POST_TODO,
  payload: data,
});

export const setLoading = value => {
  return {type: SET_LOADING, value};
};

export const getTodos = () => async dispatch => {
  dispatch(setLoading(true));
  await axios
    .get('/todos')
    .then(res => {
      if (res.data.results.length > 0) {
        dispatch(saveTodos(res.data.results));
        dispatch(setLoading(false));
      }
    })
    .catch(err => {
      dispatch(setLoading(false));
    });
};

export const postTodo = data => async dispatch => {
  await axios
    .post('/todos', data)
    .then(res => {
      Alert.alert('Success', 'Successfully created new item');
      dispatch(addTodo(data));
    })
    .catch(err => {});
};
export const editTodo = (data, id) => async dispatch => {
  await axios
    .patch(`/todos/${id}`, data)
    .then(res => {
      Alert.alert('Success', 'Successfully updated item');
    })
    .catch(err => {});
};

export const deleteTodo = id => async dispatch => {
  await axios
    .delete(`/todos/${id}`)
    .then(res => {
      Alert.alert('Success', 'Successfully deleted item');
    })
    .catch(err => {});
};
