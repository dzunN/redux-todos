import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import Todos from './container';
import {Persistor, store} from './store';

const MainApp = () => {
  return (
    <PersistGate loading={null} persistor={Persistor}>
      <SafeAreaView style={styles.root}>
        <Todos />
      </SafeAreaView>
    </PersistGate>
  );
};

function App() {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
}

export default App;

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#04103A',
    flex: 1,
  },
});
