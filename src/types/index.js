export const FETCH_TODO_SUCCESS = '@FETCH_TODO_SUCCESS';
export const POST_TODO = '@POST_TODO';
export const EDIT_TODO = '@EDIT_TODO';
export const DELETE_TODO = '@DELETE_TODO';
export const SET_LOADING = '@SET_LOADING';

export const SET_DETAIL = '@SET_DETAIL';
